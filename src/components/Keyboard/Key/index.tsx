import React, { useEffect, useRef, useState } from "react";
import { SYNTH } from "../constants";
import { Note } from "../types";
import {
  EmptyKeyStyled,
  KeyStyled,
  PrimaryTextStyled,
  SecondaryTextStyled,
} from "./styles";

const Key = ({ note }: { note: Note }) => {
  const noteString = `${note.naturalNote}${note.sharp ?? ""}${note.pitch}`;

  const [isKeyHeld, setKeyHeld] = useState(false);
  const keyElement = useRef<HTMLDivElement>(null);

  useEffect(() => {
    /**
     * Register key events to the `isKeyHeld` state
     */
    if (!note.keyboardKey) {
      return () => {};
    }

    const keyDown = (event: KeyboardEvent) => {
      event.preventDefault();
      if (!isKeyHeld && event.key === note.keyboardKey) {
        setKeyHeld(true);
      }
    };
    const keyUp = (event: KeyboardEvent) => {
      event.preventDefault();
      if (event.key === note.keyboardKey) {
        setKeyHeld(false);
      }
    };
    const mouseUp = () => setKeyHeld(false);
    const mouseDown = () => setKeyHeld(true);
    const pointerOut = mouseUp;

    window.addEventListener("keydown", keyDown);
    window.addEventListener("keyup", keyUp);
    keyElement.current?.addEventListener("mousedown", mouseDown);
    keyElement.current?.addEventListener("mouseup", mouseUp);
    keyElement.current?.addEventListener("pointerout", pointerOut);

    return () => {
      window.removeEventListener("keydown", keyDown);
      window.removeEventListener("keyup", keyUp);
      keyElement.current?.removeEventListener("mousedown", mouseDown);
      keyElement.current?.removeEventListener("mouseup", mouseUp);
      keyElement.current?.removeEventListener("pointerout", pointerOut);
    };
  }, []);

  useEffect(() => {
    if (isKeyHeld) {
      SYNTH.triggerAttack(noteString);
    } else {
      SYNTH.triggerRelease(noteString);
    }
  }, [isKeyHeld]);

  return (
    <>
      {note.keyboardKey ? (
        <KeyStyled
          isSharp={!!note.sharp}
          isKeyHeld={isKeyHeld}
          tabIndex={-1}
          ref={keyElement}
        >
          <PrimaryTextStyled>{note.keyboardKey}</PrimaryTextStyled>
          <SecondaryTextStyled>{noteString}</SecondaryTextStyled>
        </KeyStyled>
      ) : (
        <EmptyKeyStyled />
      )}
    </>
  );
};

export default Key;
