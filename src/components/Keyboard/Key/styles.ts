import styled, { css } from "styled-components";

interface KeyStyledProps {
  isKeyHeld: boolean;
  isSharp: boolean;
}

const BaseTextStyled = css`
  font: inherit;
`;

export const PrimaryTextStyled = styled.p`
  ${BaseTextStyled}

  margin: auto auto 0 10px;
  font-size: 1rem;
`;
export const SecondaryTextStyled = styled.p`
  ${BaseTextStyled}

  margin: 0 10px auto 0;
  font-size: 0.6rem;
  color: grey;
`;

const BaseKeyStyled = css`
  width: 50px;
  height: 50px;

  transform: skew(-14deg) rotateX(-180deg) rotate(-74deg);
`;

export const EmptyKeyStyled = styled.div`
  ${BaseKeyStyled}
`;

export const KeyStyled = styled.div<KeyStyledProps>`
  ${BaseKeyStyled}

  border-radius: 50%;
  user-select: none;

  border: 3px solid #000;
  display: inline-flex;

  color: grey;
  font: inherit;
  font-weight: bold;
  overflow: hidden;

  transition: background 0.3s ease-out;

  ${(props) => {
    if (props.isKeyHeld) {
      return `
      /* Make key presses immediate vs. key release */
      transition: 0s;
      background: maroon;

      ${PrimaryTextStyled} {
        color: black;
      }
    `;
    }
    if (props.isSharp) {
      return `
        background: black;

        ${PrimaryTextStyled} {
          color: white;
        }
      `;
    }
    return `
      background: white;

      ${PrimaryTextStyled} {
        color: black;
      }
    `;
  }}
`;
