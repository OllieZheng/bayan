import React from "react";
import { KEYBOARD_LAYOUT } from "./constants";
import Key from "./Key";
import { KeyboardStyled } from "./styles";

export default () => {
  return (
    <KeyboardStyled>
      {KEYBOARD_LAYOUT.map((note) => (
        <Key note={note} />
      ))}
    </KeyboardStyled>
  );
};
