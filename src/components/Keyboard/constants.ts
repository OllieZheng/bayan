import { PolySynth } from "tone";
import { NaturalNote, Note } from "./types";

export const SYNTH = new PolySynth().toDestination();

export const KEYBOARD_LAYOUT: Note[] = [
  {},
  { naturalNote: NaturalNote.A, sharp: "#", pitch: 3, keyboardKey: "a" },
  { naturalNote: NaturalNote.B, pitch: 3, keyboardKey: "z" },
  { naturalNote: NaturalNote.C, pitch: 4, keyboardKey: "w" },
  { naturalNote: NaturalNote.C, sharp: "#", pitch: 4, keyboardKey: "s" },
  { naturalNote: NaturalNote.D, pitch: 4, keyboardKey: "x" },
  { naturalNote: NaturalNote.D, sharp: "#", pitch: 4, keyboardKey: "e" },
  { naturalNote: NaturalNote.E, pitch: 4, keyboardKey: "d" },
  { naturalNote: NaturalNote.F, pitch: 4, keyboardKey: "c" },
  { naturalNote: NaturalNote.F, sharp: "#", pitch: 4, keyboardKey: "r" },
  { naturalNote: NaturalNote.G, pitch: 4, keyboardKey: "f" },
  { naturalNote: NaturalNote.G, sharp: "#", pitch: 4, keyboardKey: "v" },
  { naturalNote: NaturalNote.A, pitch: 4, keyboardKey: "t" },
  { naturalNote: NaturalNote.A, sharp: "#", pitch: 4, keyboardKey: "g" },
  { naturalNote: NaturalNote.B, pitch: 4, keyboardKey: "b" },
  { naturalNote: NaturalNote.C, pitch: 5, keyboardKey: "y" },
  { naturalNote: NaturalNote.C, sharp: "#", pitch: 5, keyboardKey: "h" },
  { naturalNote: NaturalNote.D, pitch: 5, keyboardKey: "n" },
  { naturalNote: NaturalNote.D, sharp: "#", pitch: 5, keyboardKey: "u" },
  { naturalNote: NaturalNote.E, pitch: 5, keyboardKey: "j" },
  { naturalNote: NaturalNote.F, pitch: 5, keyboardKey: "m" },
  { naturalNote: NaturalNote.F, sharp: "#", pitch: 5, keyboardKey: "i" },
  { naturalNote: NaturalNote.G, pitch: 5, keyboardKey: "k" },
  { naturalNote: NaturalNote.G, sharp: "#", pitch: 5, keyboardKey: "," },
  { naturalNote: NaturalNote.A, pitch: 5, keyboardKey: "o" },
  { naturalNote: NaturalNote.A, sharp: "#", pitch: 5, keyboardKey: "l" },
  { naturalNote: NaturalNote.B, pitch: 5, keyboardKey: "." },
  { naturalNote: NaturalNote.C, pitch: 6, keyboardKey: "p" },
  { naturalNote: NaturalNote.C, sharp: "#", pitch: 6, keyboardKey: ";" },
  { naturalNote: NaturalNote.D, pitch: 6, keyboardKey: "/" },
  { naturalNote: NaturalNote.D, sharp: "#", pitch: 6, keyboardKey: "[" },
  { naturalNote: NaturalNote.E, pitch: 6, keyboardKey: "'" },
];
