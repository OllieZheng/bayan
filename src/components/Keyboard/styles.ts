import styled from "styled-components";

export const KeyboardStyled = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 50px);

  transform: skew(14deg) rotateX(-180deg) rotate(-90deg);
  transform-origin: top left;
  gap: 0 5px;
`;
