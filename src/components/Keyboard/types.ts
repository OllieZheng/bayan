export enum NaturalNote {
  C = "C",
  D = "D",
  E = "E",
  F = "F",
  G = "G",
  A = "A",
  B = "B",
}
export type Sharp = "#" | "b";
export interface Note {
  naturalNote?: NaturalNote;
  sharp?: Sharp;
  pitch?: number;
  keyboardKey?: string;
}
