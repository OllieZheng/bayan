import * as React from "react";
import loadable from "@loadable/component";
import styled from "styled-components";
import "@fontsource/roboto-serif";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";
import { useRef } from "react";

const Keyboard = loadable(() => import("../components/Keyboard"));

const PageStyled = styled.main`
  font-family: "Roboto Serif";

  padding: 3rem;
`;
const HeaderStyled = styled.h1`
  &::first-letter {
    color: maroon;
    text-decoration: underline;
  }
`;
const GitLabLinkStyled = styled.a`
  color: maroon;
`;

const IndexPage = () => {
  const gitlab = useRef(null);

  const startGitlabBounce = () => {
    const icon = gitlab.current;
    icon.classList.add("fa-bounce");
  };
  const stopGitlabBounce = () => {
    const icon = gitlab.current;
    icon.classList.remove("fa-bounce");
  };

  return (
    <PageStyled>
      <title>Bayan</title>

      <HeaderStyled>Bayan</HeaderStyled>
      <Keyboard />

      <footer>
        <p>
          🪗 source at{" "}
          <GitLabLinkStyled href="https://gitlab.com/OllieZheng/bayan">
            <FontAwesomeIcon
              icon={faGitlab}
              ref={gitlab}
              onMouseOver={startGitlabBounce}
              onMouseOut={stopGitlabBounce}
            />
          </GitLabLinkStyled>
        </p>
      </footer>
    </PageStyled>
  );
};

export default IndexPage;
