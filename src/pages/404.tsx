import * as React from "react";
import styled from "styled-components";

const ImageContainerStyled = styled.div`
  position: relative;
  overflow: hidden;

  font-family: monospace;
  font-weight: bold;

  width: 21rem;
  padding-bottom: 2rem;
`;
const ImageBackgroundStyled = styled.div`
  position: absolute;
  top: -10px;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: -1;
`;
const PageStyled = styled.div`
  color: #232129;
  padding: 96px;
  font-family: Arial, Helvetica, sans-sarif;
`;
const HeadingStyled = styled.h1`
  margin-top: 0;
  max-width: 320px;
`;
const ParagraphStyled = styled.p``;

const NotFoundPage = () => {
  return (
    <PageStyled>
      <title>Not found</title>
      <ImageContainerStyled>
        <ImageBackgroundStyled>
          {[...Array(150)].map(() => "404... ")}
        </ImageBackgroundStyled>
        <img
          src="https://c.tenor.com/FlpBmxEO-qEAAAAj/the-simpsons-homer-simpson.gif"
          alt="funny animation GIF"
        />
      </ImageContainerStyled>
      <HeadingStyled>404...</HeadingStyled>
      <ParagraphStyled>Nothing interesting here...</ParagraphStyled>
    </PageStyled>
  );
};

export default NotFoundPage;
